import { Component, OnInit } from '@angular/core';

import { ChatServiceService } from 'src/app/services/chat-service.service'

@Component({
  selector: 'app-chat-form',
  templateUrl: './chat-form.component.html',
  styleUrls: ['./chat-form.component.css']
})
export class ChatFormComponent implements OnInit {

  message: string = "";

  constructor(private chat: ChatServiceService) { }

  ngOnInit(): void {
  }

  send() {
    this.chat.sendMessage$(this.message);
    this.chat.readUserMsg();
    this.message='';
  }

  handleSubmit(eve:any) {
    if (eve.keycode === 13) {
      this.send();
    }
  }

}
