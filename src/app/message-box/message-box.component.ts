import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { ChatServiceService } from '../services/chat-service.service';

@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.css']
})
export class MessageBoxComponent implements OnInit {

  isOwnMessage: boolean = false;

  constructor(public chat:ChatServiceService, public auth:AuthServiceService) { }

  ngOnInit(): void {
  }

  scrollToBottom() {
    debugger;
    let scrollElement = document.getElementsByClassName('scroll')[0];
    scrollElement.scrollTop = scrollElement.scrollHeight;
  }

}
