import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User$ } from '../models/chatMessage';
import {AuthServiceService} from '../services/auth-service.service'

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {

  userSubmitted = false;

  constructor(private fb: FormBuilder, private authService: AuthServiceService,
              private router:Router) { }

    //Forms declaration:
    signUpForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
      displayName: ['', [Validators.required, Validators.minLength(4)]],
    },  {validators: this.passwordMatchingValidator});

    passwordMatchingValidator (group:FormGroup) {
      return group.get('password')?.value === group.get('confirmPassword')?.value ? null :  { notmatched: true }
    }


  ngOnInit(): void {

  }

  get email() {
    return this.signUpForm.get('email') as FormControl;
  }

  get password() {
    return this.signUpForm.get('password') as FormControl;
  }

  get confirmPassword() {
    return this.signUpForm.get('confirmPassword') as FormControl;
  }

  signupSubmit() {
    const signUpData:User$  = this.signUpForm.value;
    this.authService.signUpInfo(signUpData);
    this.router.navigate(['login']);
  }

}
