import { Component, OnInit } from '@angular/core';
import {ChatServiceService} from '../services/chat-service.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {


  constructor(public chat: ChatServiceService) { }

  ngOnInit(): void {
  }

}
