export class ChatMessage$ {
    $key?: string;
    emailFrom?: string;
    emailTo?: string;
    userName?: string;
    message?: string;
    timeSent?: Date = new Date();
}

export class User$ {

    //uid?: string;
    email?: string;
    username?: string;
    password?: string;
    confirmPassword?: string;
    status?: string;
}
