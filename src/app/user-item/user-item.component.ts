import { Component, Input, OnInit } from '@angular/core';
import { User$ } from '../models/chatMessage';
import { ChatServiceService } from '../services/chat-service.service';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {

  constructor(public chat: ChatServiceService) { }

  currentUser: string = "";


  ngOnInit(): void {
  }

  @Input() usersBinding: any;

  userDataDisplay(eve: any, email: any) {debugger;
    console.log(eve, email);
    this.chat.currentUser = eve;
    this.chat.currentUserEmailTo = email;
    this.chat.userChanged(email)
  }

}
