import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthServiceService } from '../services/auth-service.service';
import { ChatServiceService } from '../services/chat-service.service';
import {User$} from '../models/chatMessage'

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {


  userSubmitted = false;

  user$:any[] = [];

  constructor(private fb: FormBuilder, private authService: AuthServiceService,
              private router: Router, private chat: ChatServiceService) { }

    //Forms declaration:
    logInForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });


  ngOnInit(): void {
    this.authService.getUserData().subscribe((x) => {
      this.user$ = x.map((y) => {
        let id =y.payload.doc.id;
        let obj:any= y.payload.doc.data();
        return { id, ...obj }
      })
      //console.log(this.user$)
      this.chat.users = this.user$;
    })
  }

  get email() {
    return this.logInForm.get('email') as FormControl;
  }

  get password() {
    return this.logInForm.get('password') as FormControl;
  }


  loginSubmit() {
    //const loginData = this.logInForm.value;
    if(this.user$){
      debugger;
      let matchEmailData = this.user$.filter((x) => {debugger;
        return x.email === this.logInForm.get('email')?.value;

      })

      if(matchEmailData.length > 0) {
        debugger
        let matchPassword = matchEmailData.filter((x) => {debugger;
          return x.password === this.logInForm.get('password')?.value;
        });
        console.log(matchPassword);
        if(matchPassword.length > 0){
          //alert("log in successfully")
          this.authService.displayLogin = false;
          this.authService.displaySignup = false;
          this.authService.displayLogout = true;
          this.authService.displayUserEmail = matchPassword[0].email;
          this.authService.displayUserName = matchPassword[0].displayName;
          this.router.navigate(['chat']);
        }else {
          alert("please enter valid password")
        }
      }else {
        alert ("please register first");
        this.logInForm.reset();
      }
    }else {
      alert("server error");
    }


  }
}
