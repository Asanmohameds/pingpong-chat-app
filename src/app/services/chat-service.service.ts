import { Injectable } from '@angular/core';

//import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
//import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { AuthServiceService } from '../services/auth-service.service';
//import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { ChatMessage$, User$ } from 'src/app/models/chatMessage';
declare var $: any;
@Injectable({
  providedIn: 'root',
})
export class ChatServiceService {
  users: User$[] = []; // this users will get value from Login component's ng-Onit block.
  currentUser: User$[] = []; // this value get from User-item comonenet's userDataDisplay().

  currentUserEmailTo: string = '';

  readMessage: any = [];
  scrollElement: any;

  scrollToBottom() {
    debugger;
    this.scrollElement.scrollTop = this.scrollElement.scrollHeight;
  }

  constructor(
    private firestore: AngularFirestore,
    private authService: AuthServiceService
  ) {}

  sendMessage$(msg: string) {
    const timestamp = new Date().toLocaleString();
    const emailFrom = this.authService.displayUserEmail;
    const emailTo = this.currentUserEmailTo;
    //const email = this.user.email;

    return this.firestore.collection('chatData').add({
      message: msg,
      timeSent: timestamp,
      emailFrom: emailFrom,
      emailTo: emailTo,
      userName: this.authService.displayUserName,
    });
  }

  getMessages() {
    return this.firestore.collection('chatData').snapshotChanges();
  }

  readUserMsg() {
    this.getMessages().subscribe((x) => {
      this.readMessage = x.map((y) => {
        let id = y.payload.doc.id;
        let obj: any = y.payload.doc.data();
        return { id, ...obj };
      });
      console.log(this.readMessage);
      let chatMessages = this.readMessage.filter((x: any) => {
        return (
          (x.emailFrom == this.authService.displayUserEmail &&
            x.emailTo == this.currentUserEmailTo) ||
          (x.emailFrom == this.currentUserEmailTo &&
            x.emailTo == this.authService.displayUserEmail)
        );
      }) as any[];

      this.readMessage = chatMessages.sort(
        (a, b) => (new Date(a.timeSent) as any) - (new Date(b.timeSent) as any)
      );


    setTimeout(() => {
      this.scrolToBottom();
    }, 500);

    });

  }

  userChanged(c: string) {
    debugger;
    this.currentUserEmailTo = c;
    this.readMessage = [];
    this.readUserMsg();
  }

  getTimeStamp(times: string) {
    const now = new Date(times);
    return now.toLocaleDateString() + ' : ' + now.toLocaleTimeString();

  }

  scrolToBottom() {

    $(".message-box").animate({ scrollTop :($('.message-box')[0].scrollHeight) }, 500);

  }

}
