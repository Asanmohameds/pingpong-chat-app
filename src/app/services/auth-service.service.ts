import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  public displayLogin:boolean = true;
  public displaySignup:boolean = true;
  public displayLogout:boolean = false;
  public displayUserEmail: string | undefined ="";
  public displayUserName: string | undefined ="";

  constructor(private firestore: AngularFirestore) { }


  signUpInfo(data: any) {
    return this.firestore.collection('userData').add(data)
  }

  getUserData() {
    return this.firestore.collection('userData').snapshotChanges();
  }

}
