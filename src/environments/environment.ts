// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebaseConfig : {
    apiKey: "AIzaSyB9G53MJ5QYsC_085EVvXAxV7-Tng40PL4",
    authDomain: "ping-pong-e087b.firebaseapp.com",
    projectId: "ping-pong-e087b",
    storageBucket: "ping-pong-e087b.appspot.com",
    messagingSenderId: "95372715544",
    appId: "1:95372715544:web:b115362a5532c01759056d"
  }

};